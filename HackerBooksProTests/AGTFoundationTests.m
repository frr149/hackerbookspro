//
//  AGTFoundationTests.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 29/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "NSUserDefaults+Reset.h"

@interface AGTFoundationTests : XCTestCase

@end

@implementation AGTFoundationTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
-(void) testThatAbsoluteStringReturnsTheURLofNSURL{
    
    NSString *str = @"http://www.agbo.biz";
    NSURL *url = [NSURL URLWithString:str];
    
    XCTAssertEqualObjects(str, [url absoluteString]);
    XCTAssertEqualObjects(url, [NSURL URLWithString:str]);
    
}
-(void) testResetOfNSUserDefaults{
    
    NSURL *url = [NSURL URLWithString:@"http://www.keepcoding.io"];
    NSString *key = @"newURL";
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setURL:url
         forKey:key];
    
    [def synchronize];
    
    [def resetAplicationDomainValues];
    
    XCTAssertNil([def URLForKey:key]);
    def = [NSUserDefaults standardUserDefaults];
    XCTAssertNil([def URLForKey:key]);
    
}

-(void) testSetMember{
    
    NSMutableSet *s = [NSMutableSet setWithArray:@[@1, @2]];
    XCTAssertEqual(2, s.count);
    [s removeObject:@1];
    NSNumber *rc = [s member:@1];
    
    XCTAssertNil(rc);
    XCTAssertEqual(1, s.count);
}

-(void) testFilterPredicateByString{
    NSArray *a = @[@"Dyanna Gregory", @"Contributors", @"Trinna Chiasson"];
    a = [a filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"'Contributors' != self"]];
    XCTAssertEqual(a.count, 2);
    
}
@end
