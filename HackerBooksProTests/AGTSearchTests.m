//
//  AGTSearchTests.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 24/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AGTCoreDataTests.h"
#import "AGTBook.h"
#import "AGTAuthor.h"

@interface AGTSearchTests : AGTCoreDataTests
@property (nonatomic, strong) AGTBook *book;
@end

@implementation AGTSearchTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"book" withExtension:@"json"]];
    
    NSError *err = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    
    // Da book
    self.book = [AGTBook bookWithJSONDictionary:dict
                                        context:self.context];
    
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void) testAuthorsPredicate{
    
    NSString *text = @"na";
    
    
    NSPredicate *authors = [NSPredicate predicateWithFormat:@"ANY authors.fullName CONTAINS[cd] %@", text];
    
    NSArray *filtered = [@[self.book] filteredArrayUsingPredicate:authors];
    
    XCTAssertEqual(filtered.count, 1);
    
}

-(void) testTitlePredicate{
    
    NSString *text = @"ata";
    
    
    NSPredicate *title = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@", text];
    
    
    
    
    NSArray *filtered = [@[self.book] filteredArrayUsingPredicate:title];
    
    XCTAssertEqual(filtered.count, 1);
    
}


@end
