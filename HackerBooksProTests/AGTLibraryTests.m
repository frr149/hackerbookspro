//
//  AGTLibraryTests.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 27/05/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AGTCoreDataTests.h"
#import "AGTTag.h"
#import "AGTAuthor.h"
#import "AGTBook.h"
#import "AGTLibrary.h"
#import "AGTBookTag.h"

@interface AGTLibraryTests : AGTCoreDataTests
@property (nonatomic, strong) AGTLibrary *lib;
@property (nonatomic, strong) NSArray *jsonDicts;
@end

@implementation AGTLibraryTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.lib = [AGTLibrary new];
    
    NSBundle *b = [NSBundle bundleForClass:[self class]];
    NSURL *url = [b URLForResource:@"library"
                     withExtension:@"json"];
    NSData *d = [NSData dataWithContentsOfURL:url];
    self.jsonDicts = [NSJSONSerialization JSONObjectWithData:d
                                                     options:0
                                                       error:nil];
    NSAssert(self.jsonDicts, @"can't be nil");
    
    [self.lib performSelector:@selector(importFromJSONData:)
                   withObject:self.jsonDicts];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    self.lib = nil;
    
}


-(void) testTagsAreSorted{
    
    NSArray *posiblySorted = [NSArray arrayWithArray:[self.lib tags]];
    NSArray *sorted = [[NSArray arrayWithArray:[self.lib tags]] sortedArrayUsingSelector:@selector(compare:)];
    
    XCTAssertEqualObjects(sorted, posiblySorted);
    
}

-(void) testTotalBookTags{
    
    NSFetchRequest *r = [self.lib bookTagsFetchRequest];
    
    XCTAssertNotNil(r);
    
    NSError *err;
    NSArray *res = [self.lib.context executeFetchRequest:r
                                               error:&err];
    XCTAssertNil(err);
    
    XCTAssertNotNil(res);
    XCTAssertEqual(80, res.count);
    
}


-(void) testLastBookTag{
    
    NSFetchRequest *r = [self.lib bookTagsFetchRequest];
    
    XCTAssertNotNil(r);
    
    NSError *err;
    NSArray *res = [self.lib.context executeFetchRequest:r
                                                   error:&err];
    XCTAssertNil(err);
    
    XCTAssertNotNil(res);
    
    AGTBookTag *last = [res lastObject];
    
    XCTAssertEqualObjects(last.tag.name, @"Web-Design");
    
}

-(void) testTotalNumberOfTagsAndBooks{
    
    NSMutableSet *collected = [NSMutableSet set];
    
 
    NSFetchRequest *r = [self.lib bookTagsFetchRequest];
    
    XCTAssertNotNil(r);
    
    NSError *err;
    NSArray *res = [self.lib.context executeFetchRequest:r
                                                   error:&err];
    XCTAssertNil(err);
    
    XCTAssertNotNil(res);

    
    
    collected = [NSMutableSet setWithArray:[res valueForKey:AGTBookTagRelationships.tag]];
    XCTAssertEqual(collected.count, 64);
    
    collected = [NSMutableSet setWithArray:[res valueForKey:AGTBookTagRelationships.book]];
    XCTAssertEqual(collected.count, 30);
}

-(void) testThatAllBookTagsHaveBookAndTag{
    
    
    NSFetchRequest *r = [self.lib bookTagsFetchRequest];
    
    XCTAssertNotNil(r);
    
    NSError *err;
    NSArray *res = [self.lib.context executeFetchRequest:r
                                                   error:&err];
    XCTAssertNil(err);
    
    for (AGTBookTag *each in res) {
        XCTAssertNotNil(each.tag);
        XCTAssertNotNil(each.book);
        
        XCTAssertNotNil(each.tag.name);
        XCTAssertNotNil(each.book.title);

    }

}

@end
