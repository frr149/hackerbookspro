//
//  AGTBookTests.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 29/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "AGTCoreDataTests.h"
#import "AGTTag.h"
#import "AGTAuthor.h"
#import "AGTBook.h"
#import "AGTPDF.h"
#import "AGTBookTag.h"

@interface AGTBookTests : AGTCoreDataTests

@end

@implementation AGTBookTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void) testTagCreation{
    
    NSArray *strings = @[@"favorite", @"Data Structures", @"Algorithms"];
    
    NSArray *tags = [AGTTag arrayOfTagsWithArrayOfStrings:strings
                                                  context:self.context];
    
    // Size
    XCTAssertEqual(strings.count, tags.count);
    
    // Type
    BOOL rc = [[tags objectAtIndex:0] isKindOfClass:[_AGTTag class]];
    NSLog(@"Name of class: %@", NSStringFromClass([[tags objectAtIndex:0] class]));
    
    XCTAssertTrue(rc);
    
    // Properties
    // ¡OJO! Estos tests NO están bien puesto que dependen de que el orden
    // de strings se preserva en tags. No tiene por qué ser así.
    XCTAssertEqualObjects(@"Favorite", [[tags objectAtIndex:0] name]);
    XCTAssertEqualObjects(@"Data Structures", [[tags objectAtIndex:1] name]);
    XCTAssertEqualObjects(@"Algorithms", [[tags objectAtIndex:2] name]);
    
    XCTAssertTrue([[tags objectAtIndex:0]isFavorite]);
    
}


-(void) testAuthorCreation{
    
    NSArray *strings = @[@"Dr Seuss", @"gerald jay sussman", @"Hal Abelson"];
    NSArray *authors = [AGTAuthor arrayOfAuthorsWithArrayOfStrings:strings
                                                           context:self.context];
    
    XCTAssertEqual(strings.count, authors.count);
    
    // Properties
    // ¡OJO! Estos tests NO están bien puesto que dependen de que el orden
    // de strings se preserva en authors. No tiene por qué ser así.
    XCTAssertEqualObjects(@"Dr Seuss", [[authors objectAtIndex:0] fullName]);
    XCTAssertEqualObjects(@"Gerald Jay Sussman", [[authors objectAtIndex:1] fullName]);
    XCTAssertEqualObjects(@"Hal Abelson", [[authors objectAtIndex:2] fullName]);
    
    
    
}

-(void) testPDFCreation{
    
    NSString *urlString = @"http://orm-atlas2-prod.s3.amazonaws.com/pdf/13a07b19e01a397d8855c0463d52f454.pdf";
    AGTPDF *pdf = [AGTPDF pdfWithRemoteURL:
                   [NSURL URLWithString:urlString]
                                   context:self.context];
    
    XCTAssertNotNil(pdf);
    XCTAssertEqualObjects(pdf.urlString, @"http://orm-atlas2-prod.s3.amazonaws.com/pdf/13a07b19e01a397d8855c0463d52f454.pdf");
    
    
}

-(void) testBookCreation{
    
    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"book" withExtension:@"json"]];
    
    NSError *err = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    XCTAssertNotNil(dict);
    
    // Da book
    AGTBook *book = [AGTBook bookWithJSONDictionary:dict
                                            context:self.context];
    
    XCTAssertNotNil(book);
    
    // Mis relaciones inversas (libro <<--->> tag) están bien:
    // cada tag que tengo debe tenerme como libro
    for (AGTBookTag *each in book.bookTags) {
        XCTAssertTrue([each.book isEqual:book ]);
    }
    
    // Lo mismo para autores
    for (AGTAuthor *author in book.authors) {
        XCTAssertTrue([author.books member:book]);
    }
    
    
}

-(void) testFavoriteNumbers{
    
    
    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"book" withExtension:@"json"]];
    
    NSError *err = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    XCTAssertNotNil(dict);
    
    // Da book
    AGTBook *book = [AGTBook bookWithJSONDictionary:dict
                                            context:self.context];
    
    XCTAssertNotNil(book);
    
    // Test number of tags
    XCTAssertEqual(3, book.bookTags.count);
    [book setIsFavorite:YES];
    XCTAssertEqual(3, book.bookTags.count);

    
    [book setIsFavorite:NO];
    XCTAssertEqual(2, book.bookTags.count);
    [book setIsFavorite:NO];
    XCTAssertEqual(2, book.bookTags.count);
    
    [book setIsFavorite:YES];
    XCTAssertEqual(3, book.bookTags.count);
    
    
    
    
}

-(void) testFavoriteFlag{
    
    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"book" withExtension:@"json"]];
    
    NSError *err = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    XCTAssertNotNil(dict);
    
    // Da book
    AGTBook *book = [AGTBook bookWithJSONDictionary:dict
                                            context:self.context];
    
    XCTAssertNotNil(book);

    
    XCTAssertTrue(book.isFavorite);
    
    book.isFavorite = NO;
    XCTAssertFalse(book.isFavorite);
    book.isFavorite = NO;
    XCTAssertFalse(book.isFavorite);
    
    book.isFavorite = YES;
    XCTAssertTrue(book.isFavorite);
    book.isFavorite = YES;
    XCTAssertTrue(book.isFavorite);
    
}

-(void) testBookCountIntegrity{
    
    
    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"book" withExtension:@"json"]];
    
    NSError *err = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    XCTAssertNotNil(dict);
    
    
    // Da book
    AGTBook *book = [AGTBook bookWithJSONDictionary:dict
                                            context:self.context];
    XCTAssertNotNil(book);
    
    //Fav Tag
    AGTTag *fav = [AGTTag favoriteTagInContext:self.context];
    
    XCTAssertEqual(1, fav.bookCount);
    
    book.isFavorite = NO;
    XCTAssertEqual(0, fav.bookCount);
    
    book.isFavorite = YES;
    XCTAssertEqual(1, fav.bookCount);
}

-(void) testBookCountIntegrityWhenAddingAndRemovingBooks{
    
    //Fav Tag
    AGTTag *fav = [AGTTag favoriteTagInContext:self.context];
    
    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"book" withExtension:@"json"]];
    
    NSError *err = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    XCTAssertNotNil(dict);
    
    
    // Books
    AGTBook *book = [AGTBook bookWithJSONDictionary:dict
                                            context:self.context];
    XCTAssertNotNil(book);
    
    XCTAssertEqual(1, fav.bookCount);
    
    data = [NSData dataWithContentsOfURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"book2" withExtension:@"json"]];
    
    dict = [NSJSONSerialization JSONObjectWithData:data
                                           options:NSJSONReadingMutableContainers
                                             error:&err];
    
    AGTBook *book2 = [AGTBook bookWithJSONDictionary:dict
                                             context:self.context];
    
    XCTAssertEqual(2, fav.bookCount);
    
    [self.context deleteObject:book2];
    [self.context processPendingChanges];
    
    XCTAssertEqual(1, fav.bookCount);
    
    [self.context deleteObject:book];
    [self.context processPendingChanges];
    
    
    XCTAssertEqual(0, fav.bookCount);
    
    
    
}


-(void) testAuthorsAsString{
    
    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"book" withExtension:@"json"]];
    
    NSError *err = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    XCTAssertNotNil(dict);
    
    
    // Da book
    AGTBook *book = [AGTBook bookWithJSONDictionary:dict
                                            context:self.context];
    XCTAssertNotNil(book);


    XCTAssertEqualObjects(@"Dyanna Gregory, Trinna Chiasson", book.stringWithAuthors);
    
}

-(void) testTagsAsString{
    
    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"book" withExtension:@"json"]];
    
    NSError *err = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    XCTAssertNotNil(dict);
    
    
    // Da book
    AGTBook *book = [AGTBook bookWithJSONDictionary:dict
                                            context:self.context];
    XCTAssertNotNil(book);

    XCTAssertEqualObjects(@"Data Visualization, Design, Favorite", book.stringWithTags);
    
}


@end
