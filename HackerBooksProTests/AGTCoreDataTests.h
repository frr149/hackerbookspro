//
//  AGTCoreDataTests.h
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 22/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import XCTest;
@import CoreData;


@interface AGTCoreDataTests : XCTestCase

@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSPersistentStoreCoordinator *coordinator;


@end
