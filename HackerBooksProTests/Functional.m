//
//  Functional.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 09/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;
@import XCTest;
#import "NSArray+Functional.h"

@interface Functional : XCTestCase

@end

@implementation Functional

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


-(void) testMapping{
    
    NSArray *strs = @[@"http://www.keepcoding.io", @"http://www.keepcoding.es"];
    NSArray *urls = @[[NSURL URLWithString:@"http://www.keepcoding.io"],
                      [NSURL URLWithString:@"http://www.keepcoding.es"]];
    
    NSArray *mapped = [strs arrayWithMappingBlock:^id(id element) {
        return [NSURL URLWithString:element];
    }];

    XCTAssertEqualObjects(urls, mapped);
    XCTAssertNoThrow([urls arrayWithMappingBlock:nil]);
    XCTAssertEqualObjects(urls, [urls arrayWithMappingBlock:nil]);
    
}























@end
