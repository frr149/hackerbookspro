//
//  AGTCoverBookImageTests.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 28/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTCoreDataTests.h"
#import "AGTBookCoverPhoto.h"

@interface AGTCoverBookImageTests : AGTCoreDataTests
@property (nonatomic, strong) UIImage *defaultImage;
@property (nonatomic, strong) NSURL *imgURL;

@end

@implementation AGTCoverBookImageTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    NSBundle *b = [NSBundle bundleForClass:[self class]];
    self.defaultImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[b URLForResource:@"emptyBookCover"
                                                                                 withExtension:@"png"]]];
    
    self.imgURL = [NSURL URLWithString:@"http://images-2.domain.com.au/2015/04/17/6449800/Article%20Lead%20-%20wide984870591mmyefimage.related.articleLeadwide.729x410.1mmx3n.png1429787987297.jpg-620x349.jpg"];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void) testCreationFromJSON{
    NSData *data = [NSData dataWithContentsOfURL:[[NSBundle bundleForClass:[self class]] URLForResource:@"book" withExtension:@"json"]];
    
    NSError *err = nil;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data
                                                         options:NSJSONReadingMutableContainers
                                                           error:&err];
    
    XCTAssertNotNil(dict);
    NSString *urlString = [dict objectForKey:@"image_url"];
    NSURL *url = [NSURL URLWithString:urlString];
    

    AGTBookCoverPhoto *cover = [AGTBookCoverPhoto bookCoverPhotoWithDefaultImage:nil
                                                                  remotePhotoURL:url
                                                                         context:self.context];
    XCTAssertNotNil(cover);
    
}

//-(void) testThatDisplaysDefaultImage{
//    
//    
//    
//    AGTBookCoverPhoto *p = [AGTBookCoverPhoto bookCoverPhotoWithDefaultImage:self.defaultImage
//                            
//                                                              remotePhotoURL:self.imgURL
//                                                                     context:self.context];
//    
//    XCTAssertEqualObjects(p.image, self.imgURL);
//    
//}
@end
