//
//  AGTBaseManagedObjectTests.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 24/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTBaseManagedObjectTests.h"
#import "AGTTag.h"
#import "AGTThing.h"

@implementation AGTBaseManagedObjectTests


-(void) testThatNonExistantIsCreated{
    
    NSUUID *uid = [NSUUID UUID];
    
    AGTTag *weirdTag = [AGTTag uniqueObjectWithValue:[uid UUIDString]
                                                      forKey:AGTTagAttributes.name
                                      inManagedObjectContext:self.context];
    
    XCTAssertNotNil(weirdTag);
    
}


-(void) testThatExistingIsNotCreated{
    
    // String property
    AGTTag *tag1 = [AGTTag uniqueObjectWithValue:@"Epetecan"
                                                  forKey:AGTTagAttributes.name
                                  inManagedObjectContext:self.context];
    
    AGTTag *tag2 = [AGTTag uniqueObjectWithValue:@"Epetecan"
                                                  forKey:AGTTagAttributes.name
                                  inManagedObjectContext:self.context];
    
    
    XCTAssertEqualObjects(tag1, tag2);
    XCTAssertEqual(tag1, tag2);
    
    
    // Date property
    NSDate *now = [NSDate date];
    AGTThing *t1 = [AGTThing uniqueObjectWithValue:now
                                            forKey:AGTThingAttributes.date
                            inManagedObjectContext:self.context];
    
    AGTThing *t2 = [AGTThing uniqueObjectWithValue:now
                                            forKey:AGTThingAttributes.date
                            inManagedObjectContext:self.context];
    
    XCTAssertEqualObjects(t1, t2);
    XCTAssertEqual(t1, t2);
    
    // Number property
    AGTThing *t3 = [AGTThing uniqueObjectWithValue:@42
                                            forKey:AGTThingAttributes.number
                            inManagedObjectContext:self.context];
    
    AGTThing *t4 = [AGTThing uniqueObjectWithValue:@42.0
                                            forKey:AGTThingAttributes.number
                            inManagedObjectContext:self.context];
    
    XCTAssertEqualObjects(t3, t4);
    XCTAssertEqual(t3, t4);
    
    
}



@end
