//
//  AGTCoreDataTests.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 22/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTCoreDataTests.h"
#import <objc/runtime.h>
#import "AGTThing.h"



@interface AGTCoreDataTests ()



@end



@implementation AGTCoreDataTests

-(void) setUp{
    
    [super setUp];
    
    // In Memory stack for tests
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model"
                                              withExtension:@"momd"];
    NSAssert(modelURL, @"Can't be nil");
    
    NSURL *thingURL = [[NSBundle bundleForClass:[self class]]
                       URLForResource:@"TestOnlyModel"
                       withExtension:@"momd"];
    NSAssert(thingURL, @"Can't be nil");
    
    NSManagedObjectModel *realModel = [[NSManagedObjectModel alloc]
                                       initWithContentsOfURL:modelURL];
    NSAssert(realModel, @"Can't be nil");
    
    NSManagedObjectModel *testModel = [[NSManagedObjectModel alloc]
                                       initWithContentsOfURL:thingURL];
    NSAssert(testModel, @"Can't be nil");
    
    NSManagedObjectModel *model = [NSManagedObjectModel
                                   modelByMergingModels:@[realModel, testModel]];
    NSAssert(model, @"Can't be nil");
    
    self.coordinator = [[NSPersistentStoreCoordinator alloc]
                        initWithManagedObjectModel:model];
    
    NSError *err = nil;
    NSPersistentStore *store = [self.coordinator
                                addPersistentStoreWithType:NSInMemoryStoreType
                                configuration:nil
                                URL:nil
                                options:nil
                                error:&err];
    if (!store) {
        NSLog(@"Error al crear el store: %@", err);
    }
    
    self.context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    self.context.persistentStoreCoordinator = self.coordinator;
    

}

-(void) tearDown{
    [super tearDown];
    
    self.context = nil;
    self.coordinator = nil;
    
}

-(void) testMODoesNotConformToNSCopying{
    
    XCTAssert(![NSManagedObject conformsToProtocol:@protocol(NSCopying)]);
    XCTAssert(!hasMethod([NSManagedObject class], @selector(copyWithZone:)));
    
}

-(void) testMOCEquality{
    
    // NSManagedObject implements isEqual & hash byt itself (not inherited).
    XCTAssert(hasMethod([NSManagedObject class], @selector(isEqual:)));
    XCTAssert(hasMethod([NSManagedObject class], @selector(hash)));
}



BOOL hasMethod(Class cls, SEL sel) {
    unsigned int methodCount;
    Method *methods = class_copyMethodList(cls, &methodCount);
    
    BOOL result = NO;
    for (unsigned int i = 0; i < methodCount; ++i) {
        if (method_getName(methods[i]) == sel) {
            result = YES;
            break;
        }
    }
    
    free(methods);
    return result;
}
@end
