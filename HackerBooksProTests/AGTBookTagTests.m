//
//  AGTBookTagTests.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 24/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTBookTagTests.h"
#import "AGTTag.h"
@implementation AGTBookTagTests

-(void)setUp{
    [super setUp];
}

-(void) tearDown{
    [super tearDown];
    
}


-(void) testNameCapitalization{
    
    AGTTag *tag = [AGTTag tagWithName:@"lINEar aLGebra"
                              context:self.context];
    
    AGTTag *emptyTag = [AGTTag tagWithName:@""
                                   context:self.context];
    AGTTag *singleTag = [AGTTag tagWithName:@"a"
                                    context:self.context];
    
    XCTAssertEqualObjects(@"Linear Algebra", [tag name]);
    XCTAssertEqualObjects(@"", [emptyTag name]);
    XCTAssertEqualObjects(@"A", [singleTag name]);
    
}

-(void) testOrdering{
    
    AGTTag *fav = [AGTTag favoriteTagInContext:self.context];
    
    AGTTag *other = [AGTTag tagWithName:@"shanKar Ali khan"
                                context:self.context];
    
    AGTTag *a1 = [AGTTag tagWithName:@"A1A car Wash"
                             context:self.context];
    
    // Esto en el fondo no sirve de mucho, ya que no sabemos el algoritmo
    // que va a usar SQLite para comparar cadenas. Solo se puede pedir a
    // Jobs que tenga misericordia.
    XCTAssertEqual(NSOrderedSame, [fav.proxyForSorting compare:fav.proxyForSorting], @"Should be equal");
    XCTAssertEqual(NSOrderedAscending, [fav.proxyForSorting caseInsensitiveCompare:a1.proxyForSorting], @"Favorite should always be smaller");
    XCTAssertEqual(NSOrderedDescending, [other.proxyForSorting caseInsensitiveCompare:a1.proxyForSorting], @"Shankar should always be bigger");
    XCTAssertEqual(NSOrderedDescending, [other.proxyForSorting caseInsensitiveCompare:fav.proxyForSorting],@"Favorite should always be smaller");
    
}

-(void) testSort{
    
    AGTTag *design = [AGTTag tagWithName:@"dESign"
                                 context:self.context];
    AGTTag *data = [AGTTag tagWithName:@"data visualization"
                               context:self.context];
    AGTTag *fav = [AGTTag favoriteTagInContext:self.context];
    
    NSArray *sorted = [@[fav, data, design] valueForKey:AGTTagAttributes.proxyForSorting];
    NSArray *toBeSorted = [[@[data, fav, design] valueForKey:AGTTagAttributes.proxyForSorting] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    XCTAssertEqualObjects(sorted, toBeSorted);
}

-(void)testEquality{
    
    AGTTag *a = [AGTTag tagWithName:@"Aaaaaa"
                            context:self.context];
    AGTTag *a1 = [AGTTag tagWithName:@"AaAaAa"
                             context:self.context];
    AGTTag *b = [AGTTag tagWithName:@"something different"
                            context:self.context];
    
    XCTAssertEqualObjects(a, a);
    XCTAssertEqualObjects(a, a1);
    
    XCTAssertNotEqualObjects(a, b);
    XCTAssertNotEqualObjects(a, @"b");
    
    
}

-(void) testFavoriteTag{
    AGTTag *a = [AGTTag tagWithName:@"lINEar aLGebra"
                            context:self.context];
    AGTTag *fav =[AGTTag tagWithName:@"favorite"
                             context:self.context];
    AGTTag *fav2 = [AGTTag favoriteTagInContext:self.context];
    
    XCTAssertTrue([fav isFavorite]);
    XCTAssertTrue([fav2 isFavorite]);
    XCTAssertFalse([a isFavorite]);
    
}
@end
