////
////  AGTLibraryVCTests.m
////  HackerBooksPro
////
////  Created by Fernando Rodríguez Romero on 17/06/15.
////  Copyright (c) 2015 Agbo. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import <XCTest/XCTest.h>
//#import "AGTLibrary.h"
//#import "AGTLibraryViewControllerTableViewController.h"
//
//@interface AGTLibraryVCTests : XCTestCase
//@property (nonatomic, strong) AGTLibraryViewControllerTableViewController *vc;
//@end
//
//@implementation AGTLibraryVCTests
//
//- (void)setUp {
//    [super setUp];
//    // Put setup code here. This method is called before the invocation of each test method in the class.
//    
//    
//    self.vc = [[AGTLibraryViewControllerTableViewController alloc]
//               initWithModel:[[AGTLibrary alloc] init]];
//    [self.vc viewWillAppear:NO];
//    [self.vc viewDidAppear:NO];
//    
//
//}
//
//- (void)tearDown {
//    // Put teardown code here. This method is called after the invocation of each test method in the class.
//    [super tearDown];
//    self.vc = nil;
//    
//}
//
//-(void)testNumberOfSections{
//    
//    NSInteger n = [self.vc numberOfSectionsInTableView:nil];
//    
//    XCTAssertEqual(n, 63);
//}
//
//-(void) testNameOfFirstSection{
//    
//    NSString *str = [self.vc tableView:nil titleForHeaderInSection:0];
//    XCTAssertEqualObjects(str, @"Access Controls");
//    
//    str = [self.vc tableView:nil titleForHeaderInSection:1];
//    XCTAssertEqualObjects(str, @"Algorithms");
//    
//}
//
//-(void) testNumberOfItemsInSection{
//    
//    XCTAssertEqual(1, [self.vc tableView:nil numberOfRowsInSection:0]);
//    XCTAssertEqual(3, [self.vc tableView:nil numberOfRowsInSection:1]);
//    XCTAssertEqual(1, [self.vc tableView:nil numberOfRowsInSection:62]);
//}
//
//
//
//@end
