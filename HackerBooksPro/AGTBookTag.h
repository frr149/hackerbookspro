#import "_AGTBookTag.h"
#import "AGTBook.h"
#import "AGTTag.h"

@interface AGTBookTag : _AGTBookTag {}

+(instancetype) bookTagWithBook: (AGTBook*) book
                            tag:(AGTTag*) tag
                        context:(NSManagedObjectContext*) context;

@end
