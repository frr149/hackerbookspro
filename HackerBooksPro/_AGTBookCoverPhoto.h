// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTBookCoverPhoto.h instead.

@import CoreData;
#import "AGTBaseManagedObject.h"

extern const struct AGTBookCoverPhotoAttributes {
	__unsafe_unretained NSString *data;
	__unsafe_unretained NSString *remoteURLString;
} AGTBookCoverPhotoAttributes;

extern const struct AGTBookCoverPhotoRelationships {
	__unsafe_unretained NSString *book;
} AGTBookCoverPhotoRelationships;

@class AGTBook;

@interface AGTBookCoverPhotoID : NSManagedObjectID {}
@end

@interface _AGTBookCoverPhoto : AGTBaseManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AGTBookCoverPhotoID* objectID;

@property (nonatomic, strong) NSData* data;

//- (BOOL)validateData:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong, readonly) NSString* remoteURLString;

//- (BOOL)validateRemoteURLString:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AGTBook *book;

//- (BOOL)validateBook:(id*)value_ error:(NSError**)error_;

@end

@interface _AGTBookCoverPhoto (CoreDataGeneratedPrimitiveAccessors)

- (NSData*)primitiveData;
- (void)setPrimitiveData:(NSData*)value;

- (NSString*)primitiveRemoteURLString;
- (void)setPrimitiveRemoteURLString:(NSString*)value;

- (AGTBook*)primitiveBook;
- (void)setPrimitiveBook:(AGTBook*)value;

@end
