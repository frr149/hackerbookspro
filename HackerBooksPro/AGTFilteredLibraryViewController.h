//
//  AGTFilteredLibraryViewController.h
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 24/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTCoreDataTableViewController.h"

@interface AGTFilteredLibraryViewController : AGTCoreDataTableViewController

@end
