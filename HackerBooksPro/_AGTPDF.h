// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTPDF.h instead.

@import CoreData;
#import "AGTBaseManagedObject.h"

extern const struct AGTPDFAttributes {
	__unsafe_unretained NSString *data;
	__unsafe_unretained NSString *urlString;
} AGTPDFAttributes;

extern const struct AGTPDFRelationships {
	__unsafe_unretained NSString *book;
} AGTPDFRelationships;

@class AGTBook;

@interface AGTPDFID : NSManagedObjectID {}
@end

@interface _AGTPDF : AGTBaseManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AGTPDFID* objectID;

@property (nonatomic, strong) NSData* data;

//- (BOOL)validateData:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong, readonly) NSString* urlString;

//- (BOOL)validateUrlString:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AGTBook *book;

//- (BOOL)validateBook:(id*)value_ error:(NSError**)error_;

@end

@interface _AGTPDF (CoreDataGeneratedPrimitiveAccessors)

- (NSData*)primitiveData;
- (void)setPrimitiveData:(NSData*)value;

- (NSString*)primitiveUrlString;
- (void)setPrimitiveUrlString:(NSString*)value;

- (AGTBook*)primitiveBook;
- (void)setPrimitiveBook:(AGTBook*)value;

@end
