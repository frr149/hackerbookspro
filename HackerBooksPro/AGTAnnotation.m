#import "AGTAnnotation.h"
#import "AGTAnnotationPhoto.h"

@interface AGTAnnotation ()

// Private interface goes here.

@end

@implementation AGTAnnotation

+(instancetype) annotationWithContext:(NSManagedObjectContext*) context{
    
    
    AGTAnnotation *n = [self insertInManagedObjectContext:context];
    AGTAnnotationPhoto *photo = [AGTAnnotationPhoto insertInManagedObjectContext:context];
    n.photo = photo;
    n.creationDate = [NSDate date];
    
    return n;
}

@end
