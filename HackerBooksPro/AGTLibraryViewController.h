//
//  AGTLibraryViewControllerTableViewController.h
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 11/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import UIKit;
@class AGTLibrary;
@class AGTLibraryViewController;
@class AGTBook;

@protocol AGTLibraryViewControllerDelegate <NSObject>

-(void) libraryViewController:(AGTLibraryViewController*) vc
                didSelectBook:(AGTBook*) book;

@end

@interface AGTLibraryViewController : UIViewController <UITableViewDataSource,
UITableViewDelegate, AGTLibraryViewControllerDelegate>

@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id<AGTLibraryViewControllerDelegate> delegate;


-(id) initWithModel:(AGTLibrary*) model;
@end
