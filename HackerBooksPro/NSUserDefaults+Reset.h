//
//  NSUserDefaults+Reset.h
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 29/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (Reset)

-(void) resetAplicationDomainValues;

@end
