#import "_AGTPDF.h"

#define PDF_DATA_DID_DOWNLOAD_NOTIFICATION @"PDFHASARRIVED"

@interface AGTPDF : _AGTPDF {}



// Unique by the pdf url
+(instancetype)pdfWithRemoteURL:(NSURL*) url
                        context:(NSManagedObjectContext *) context;

// Descarga, si es necesario, el pdf e informa de errores
// en caso de haberlos.
// Al usar este método para acceder a los datos, no necesitamos
// preocuparnos por si está en local o no: si no lo está, lo
// descargará y punto.
-(void) withDownloadedPDFData:(void (^)(NSData *pdfData))completionBlock;

@end
