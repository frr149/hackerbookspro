//
//  AGTAnnotationsViewController.h
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 20/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTCoreDataTableViewController.h"

@class AGTBook;

@interface AGTAnnotationsViewController : AGTCoreDataTableViewController

-(id)initWithModel:(AGTBook*) model;

@end
