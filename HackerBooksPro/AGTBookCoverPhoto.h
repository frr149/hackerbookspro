@import UIKit;

#import "_AGTBookCoverPhoto.h"

@interface AGTBookCoverPhoto : _AGTBookCoverPhoto {}

@property (nonatomic, strong, readonly) NSURL *remoteURL;
@property (nonatomic, strong, readonly) UIImage *image;

+(instancetype) bookCoverPhotoWithDefaultImage:(UIImage*) defaultImage
                                remotePhotoURL:(NSURL*) remoteURL
                                       context:(NSManagedObjectContext *) context;

@end
