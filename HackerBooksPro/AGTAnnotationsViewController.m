//
//  AGTAnnotationsViewController.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 20/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTAnnotationsViewController.h"
#import "AGTBook.h"
#import "AGTAnnotation.h"
#import "AGTAnnotationPhoto.h"
#import "AGTAnnotationViewController.h"

@interface AGTAnnotationsViewController ()
@property (nonatomic,strong) AGTBook *model;
@end

@implementation AGTAnnotationsViewController


-(id)initWithModel:(AGTBook*) model{
    
    NSFetchRequest *r = [NSFetchRequest
                         fetchRequestWithEntityName:[AGTAnnotation entityName]];
    r.sortDescriptors = @[[NSSortDescriptor
                           sortDescriptorWithKey:AGTAnnotationAttributes.creationDate
                           ascending:YES],
                          [NSSortDescriptor
                           sortDescriptorWithKey:AGTAnnotationAttributes.text
                           ascending:YES
                           selector:@selector(caseInsensitiveCompare:)]];
    
    
    r.predicate = [NSPredicate predicateWithFormat:@"book == %@", model];
    

    NSFetchedResultsController *fc = [[NSFetchedResultsController alloc]
                                      initWithFetchRequest:r
                                      managedObjectContext:model.managedObjectContext
                                      sectionNameKeyPath:nil
                                      cacheName:nil];
    
    if (self = [super initWithFetchedResultsController:fc
                                                 style:UITableViewStylePlain]) {
        _model = model;
        self.title = @"Annotations";
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addAnnotationButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table Data Source

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellID = @"AnnotationCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellID];
    }
    
    AGTAnnotation *a = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = a.text;
    cell.imageView.image = a.photo.image;
    
    return cell;
}

#pragma mark - Misc
-(void) addAnnotationButton{
    
    UIBarButtonItem *b = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                      target:self
                                                                      action:@selector(addAnnotation:)];
    
    self.navigationItem.rightBarButtonItem = b;
}



-(void) addAnnotation:(id) sender{
    
    AGTAnnotation *annotation = [AGTAnnotation
                                 annotationWithContext:self.model.managedObjectContext];
    
    annotation.book = self.model;
    
    
    AGTAnnotationViewController *avc = [[AGTAnnotationViewController alloc]
                                        initWithModel:
                                        annotation];
    
    [self.navigationController pushViewController:avc
                                         animated:YES];
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AGTAnnotation *an = [self.fetchedResultsController
                         objectAtIndexPath:indexPath];
    
    AGTAnnotationViewController *aVC = [[AGTAnnotationViewController alloc] initWithModel:an];
    
    [self.navigationController pushViewController:aVC
                                         animated:YES];
}




@end
