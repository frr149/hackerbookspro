//
//  AGTPhotoViewController.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 20/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTPhotoViewController.h"
#import "AGTAnnotationPhoto.h"

@interface AGTPhotoViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic,strong) AGTAnnotationPhoto *model;
@end

@implementation AGTPhotoViewController

- (IBAction)takePicture:(id)sender {
    
    UIImagePickerController *p = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        p.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        p.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    p.delegate = self;
    
    [self presentViewController:p
                       animated:YES
                     completion:nil];
}

-(id) initWithModel:(AGTAnnotationPhoto*) model{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = model;
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.photoView.image = self.model.image;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    self.model.image = self.photoView.image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIImagePickerControllerDelegate
-(void) imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    
    self.model.image = [info
                        objectForKey:UIImagePickerControllerOriginalImage];
    
    [self dismissViewControllerAnimated:YES
                             completion:nil];
    
    
}





@end
