//
//  AGTLibrary.h
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 01/05/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

/*
 * Encapsula todo el stack de core data. Es el modelo de biblioteca
 * y me permite, de forma sencilla, arrancar la interfaz mientras
 * el modelo aun no está listo y avisar al usuario que estamos en 
 * ello.
 *
 * Se inicializa con alloc|init o new y luego con un 
 * openWithCompletionBlock:
 *
 */

@import CoreData;

@interface AGTLibrary : NSObject

@property (readonly, nonatomic) NSArray *tags;
@property (readonly, nonatomic) NSManagedObjectContext *context;
@property (readonly, nonatomic) BOOL isOpen;
/*
 * Crea el stack de Core Data y avisa cuando está listo para
 * empezar. Algunas de las operaciones que tal vez se tengan que
 * llevar a cabo son bloqueantes, así que avisamos cuando está
 * listo mediante un completionBlock
 */

- (void)openWithCompletionHandler: (void (^)(NSError* error))completionBlock ;


-(NSFetchRequest*) bookTagsFetchRequest;

-(NSFetchRequest*) filteredBooksFetchRequestContainingText:(NSString *) text;

-(void)importFromJSONData:(NSArray *)jsonDicts;

@end
