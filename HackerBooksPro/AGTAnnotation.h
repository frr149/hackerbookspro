#import "_AGTAnnotation.h"

@interface AGTAnnotation : _AGTAnnotation {}

// Crea una nueva nota
+(instancetype) annotationWithContext:(NSManagedObjectContext*) context;

@end
