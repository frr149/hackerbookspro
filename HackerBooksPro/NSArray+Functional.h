//
//  NSArray+Functional.h
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 09/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Functional)

// Devuelve un nuevo array con los elementos del original modificados
// por el bloque
-(NSArray*)arrayWithMappingBlock:(id (^)(id element))mappingBlock;

@end
