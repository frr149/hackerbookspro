// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTAnnotation.h instead.

@import CoreData;
#import "AGTBaseManagedObject.h"

extern const struct AGTAnnotationAttributes {
	__unsafe_unretained NSString *creationDate;
	__unsafe_unretained NSString *text;
} AGTAnnotationAttributes;

extern const struct AGTAnnotationRelationships {
	__unsafe_unretained NSString *book;
	__unsafe_unretained NSString *photo;
} AGTAnnotationRelationships;

@class AGTBook;
@class AGTAnnotationPhoto;

@interface AGTAnnotationID : NSManagedObjectID {}
@end

@interface _AGTAnnotation : AGTBaseManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AGTAnnotationID* objectID;

@property (nonatomic, strong) NSDate* creationDate;

//- (BOOL)validateCreationDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* text;

//- (BOOL)validateText:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AGTBook *book;

//- (BOOL)validateBook:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AGTAnnotationPhoto *photo;

//- (BOOL)validatePhoto:(id*)value_ error:(NSError**)error_;

@end

@interface _AGTAnnotation (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveCreationDate;
- (void)setPrimitiveCreationDate:(NSDate*)value;

- (NSString*)primitiveText;
- (void)setPrimitiveText:(NSString*)value;

- (AGTBook*)primitiveBook;
- (void)setPrimitiveBook:(AGTBook*)value;

- (AGTAnnotationPhoto*)primitivePhoto;
- (void)setPrimitivePhoto:(AGTAnnotationPhoto*)value;

@end
