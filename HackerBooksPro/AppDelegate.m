//
//  AppDelegate.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 22/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

@import CoreData;

#import "AppDelegate.h"
#import "Settings.h"
#import "NSUserDefaults+Reset.h"
#import "AGTAsyncImage.h"
#import "AGTLibrary.h"
#import "AGTLibraryViewController.h"
#import "UIViewController+Navigation.h"

@interface AppDelegate ()
@property (nonatomic) BOOL isModelPreloaded;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Configuration
    [self configure];
    
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    
    AGTLibrary *lib = [AGTLibrary new];
    
    AGTLibraryViewController *libVC = [[AGTLibraryViewController alloc] initWithModel:lib ];
    libVC.delegate = libVC;
    
    self.window.rootViewController = [libVC wrappedInNavigation];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}







#pragma mark -  Configuration
-(void) configure{
    
    // Comportamiento de caches
    if (FLUSH_ALL_ASYNC_IMAGE_CACHE) {
        [AGTAsyncImage flushLocalCache];
    }
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    
    if (FLUSH_APPLICATION_DOMAIN_IN_NSUSERDEFAULTS) {
        [def resetAplicationDomainValues];
    }

    
    // Set default values for User Prefs
    if (![def objectForKey:IS_LIBRARY_IN_CORE_DATA]) {
        // Por defecto, toca descargar y guardar
        [def setBool:NO
              forKey:IS_LIBRARY_IN_CORE_DATA];
    }
    
    
    // Save, just in case
    [def synchronize];
    
    [self configureAppearance];
}


-(void) configureAppearance{
    
    // barTintColor cambia el color de la barra
    [[UINavigationBar appearance] setBarTintColor: [UIColor colorWithWhite:0.6
                                                                     alpha:1]];
    // TintColor asigna el color de los botones
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithWhite:0.9
                                                                 alpha:1]];
    
    // La Fuente
    NSShadow * shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor lightGrayColor];
    shadow.shadowOffset = CGSizeMake(1, 1);
    

    
    UIFont *navbarFont = [UIFont fontWithName:@"HelveticaNeue-CondensedBlack"
                                         size:24];
    
    NSDictionary * navBarTitleTextAttributes =
    @{ NSForegroundColorAttributeName : [UIColor colorWithWhite:0.1
                                                          alpha:1],
       NSShadowAttributeName          : shadow,
       NSFontAttributeName            : navbarFont };
    
    [[UINavigationBar appearance] setTitleTextAttributes:navBarTitleTextAttributes];
    
    
    // Barra de botones
    [[UIToolbar appearance] setBarTintColor: [UIColor colorWithWhite:0.6
                                                               alpha:1]];
    
    // TintColor asigna el color de los botones
    [[UIToolbar appearance] setTintColor:[UIColor colorWithWhite:0.9
                                                           alpha:1]];
    
    // Ahora barra de botonoes y los botones en sí
    UIFont *toolbarFont = [UIFont fontWithName:@"HelveticaNeue-Bold"
                                          size:18];
    
    NSDictionary * toolBarTitleTextAttributes =
    @{ NSForegroundColorAttributeName : [UIColor colorWithWhite:0.9
                                                          alpha:1],
       NSFontAttributeName            : toolbarFont };
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:toolBarTitleTextAttributes
                                                forState:UIControlStateNormal];
}





@end
