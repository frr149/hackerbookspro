//
//  AGTLibraryViewControllerTableViewController.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 11/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTLibraryViewController.h"
#import "AGTLibrary.h"
#import "AGTBookTag.h"
#import "AGTBook.h"
#import "AGTBookTableViewCell.h"
#import "AGTBookViewController.h"
#import "AGTFilteredLibraryViewController.h"

@interface AGTLibraryViewController ()<NSFetchedResultsControllerDelegate, UISearchControllerDelegate, UISearchResultsUpdating>
@property (nonatomic, strong) AGTLibrary *model;
@property (nonatomic, strong) UIActivityIndicatorView *loadingView;
@property (nonatomic, strong) NSFetchedResultsController *fc;


// Búsqueda
@property (nonatomic, strong) UISearchController *searchController;
@property(nonatomic, strong) AGTFilteredLibraryViewController *filteredController;


@end

@implementation AGTLibraryViewController

#pragma mark - Initialization
-(id) initWithModel:(AGTLibrary*) model{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = model;
        self.title = @"Hacker Books Pro";
    }
    return self;
}

#pragma mark - View Lifecycle
-(void) viewDidLoad{
    [super viewDidLoad];
    
    // Añadimos un activity indicator
    self.loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.loadingView.color = [UIColor lightGrayColor];
    self.loadingView.hidden = NO;
    
    [self.tableView addSubview:self.loadingView];
    self.loadingView.center = self.view.center;
    
    
    [self.loadingView startAnimating];
    
    
    // reducimos alfa de tabla
    self.tableView.alpha = 0.8;
    
    // Búsqueda
    self.filteredController = [[AGTFilteredLibraryViewController alloc]
                               initWithFetchedResultsController:nil
                               style:self.tableView.style];
    
    
    self.searchController = [[UISearchController alloc]
                             initWithSearchResultsController:self.filteredController];
    
    [self.searchController.searchBar sizeToFit];
    
    self.searchController.delegate = self;
    self.searchController.searchResultsUpdater = self;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    // para que los resultados se muestren sobre mi vista, sino lo harian sobre el rootVC
    self.definesPresentationContext = YES;
    
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self registerNib];
    
    // cargamos el modelo
    [self.model openWithCompletionHandler:^(NSError *error) {
        
        // Eliminamos el activity
        [self.loadingView stopAnimating];
        self.loadingView.hidden = YES;
        [self.loadingView removeFromSuperview];
        self.loadingView = nil;
        
        // devolvemos el alfa
        self.tableView.alpha = 1.0;
        
        // Creamos el fetchedResults
        self.fc = [[NSFetchedResultsController alloc] initWithFetchRequest:self.model.bookTagsFetchRequest
                                                      managedObjectContext:self.model.context
                                                        sectionNameKeyPath:@"tag.name"
                                                                 cacheName:nil];
        
        NSError *err;
        BOOL rc = [self.fc performFetch:&err];
        
        
        if (!rc) {
            NSLog(@"Error while performing fetch: %@", self.model.bookTagsFetchRequest);
        }
        
        // Asignamos delegado y data source para que empiece a
        // cargar datos
        self.fc.delegate = self;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self.tableView reloadData];
        
    } ];
    
    
    
}


-(void) registerNib{
    
    
    UINib *nib = [UINib nibWithNibName:@"AGTBookTableViewCell"
                                bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:[AGTBookTableViewCell cellId]];
    
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    NSInteger n = [[self.fc sections] count];
    return n;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    NSInteger n = [[self.fc.sections objectAtIndex:section] numberOfObjects];
    return n;
}

-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *n = [[self.fc.sections objectAtIndex:section] name];
    return n;
}


-(UITableViewCell*) tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    // Obtener el BookTag
    AGTBookTag *bt = [self.fc objectAtIndexPath:indexPath];
    
    
    // Obtner el Book
    AGTBook *b = bt.book;
    
    NSLog(@"Reading Book: %@", bt.book);
    
    // Crear la celda
    AGTBookTableViewCell *cell = [tableView
                                  dequeueReusableCellWithIdentifier:[AGTBookTableViewCell cellId]
                                  forIndexPath:indexPath];
    
    
    
    // Configurarla
    [cell observeBook:b];
    
    return cell;
    
}

-(CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [AGTBookTableViewCell height];
}




#pragma mark - NSFetchedResultsControllerDelegate
-(void) controllerWillChangeContent:(NSFetchedResultsController *)controller{
    [self.tableView beginUpdates];
}

-(void) controller:(NSFetchedResultsController *)controller
  didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type{
    
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            break;
    }
    
}

-(void) controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath{
    
    
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:@[indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
            
            [self.tableView insertRowsAtIndexPaths:@[newIndexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
            break;
        default:
            break;
    }
    
}

-(void) controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.tableView endUpdates];
}




#pragma mark - Table Delegate
-(void) tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // El BookTag
    AGTBookTag *bt = [self.fc objectAtIndexPath:indexPath];
    
    // El book
    AGTBook *b = bt.book;
    
    // Avisamos al delegado
    [self.delegate libraryViewController:self
                           didSelectBook:b];
    
}




#pragma mark - AGTLibraryViewControllerDelegate
-(void) libraryViewController:(AGTLibraryViewController *)vc
                didSelectBook:(AGTBook *)book{
    
    AGTBookViewController *bvc = [[AGTBookViewController alloc]
                                  initWithModel:book];
    
    [self.navigationController pushViewController:bvc
                                         animated:YES];
    
}


#pragma mark - UISearchResultsUpdating
-(void)updateSearchResultsForSearchController:(UISearchController *)searchController{
    
    // Obtener el texto
    NSString *text = searchController.searchBar.text;
    
    // Obtener el fetchRequest
    NSFetchRequest *r = [self.model filteredBooksFetchRequestContainingText:text];
    
    // Crear un FetchedResults
    NSFetchedResultsController *fc = [[NSFetchedResultsController alloc]
                                      initWithFetchRequest:r
                                      managedObjectContext:self.model.context
                                      sectionNameKeyPath:nil
                                      cacheName:nil];
    
    
    // Endosárselo al self.filtered
    self.filteredController.fetchedResultsController = fc;
    
    // Rezar un Ave María a Santa Tecla
    
}
@end
