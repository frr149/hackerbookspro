// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTBook.h instead.

@import CoreData;
#import "AGTBaseManagedObject.h"

extern const struct AGTBookAttributes {
	__unsafe_unretained NSString *lastRead;
	__unsafe_unretained NSString *title;
} AGTBookAttributes;

extern const struct AGTBookRelationships {
	__unsafe_unretained NSString *annotations;
	__unsafe_unretained NSString *authors;
	__unsafe_unretained NSString *bookTags;
	__unsafe_unretained NSString *coverPhoto;
	__unsafe_unretained NSString *pdf;
} AGTBookRelationships;

@class AGTAnnotation;
@class AGTAuthor;
@class AGTBookTag;
@class AGTBookCoverPhoto;
@class AGTPDF;

@interface AGTBookID : NSManagedObjectID {}
@end

@interface _AGTBook : AGTBaseManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AGTBookID* objectID;

@property (nonatomic, strong) NSDate* lastRead;

//- (BOOL)validateLastRead:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong, readonly) NSString* title;

//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *annotations;

- (NSMutableSet*)annotationsSet;

@property (nonatomic, strong) NSSet *authors;

- (NSMutableSet*)authorsSet;

@property (nonatomic, strong) NSSet *bookTags;

- (NSMutableSet*)bookTagsSet;

@property (nonatomic, strong) AGTBookCoverPhoto *coverPhoto;

//- (BOOL)validateCoverPhoto:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AGTPDF *pdf;

//- (BOOL)validatePdf:(id*)value_ error:(NSError**)error_;

@end

@interface _AGTBook (AnnotationsCoreDataGeneratedAccessors)
- (void)addAnnotations:(NSSet*)value_;
- (void)removeAnnotations:(NSSet*)value_;
- (void)addAnnotationsObject:(AGTAnnotation*)value_;
- (void)removeAnnotationsObject:(AGTAnnotation*)value_;

@end

@interface _AGTBook (AuthorsCoreDataGeneratedAccessors)
- (void)addAuthors:(NSSet*)value_;
- (void)removeAuthors:(NSSet*)value_;
- (void)addAuthorsObject:(AGTAuthor*)value_;
- (void)removeAuthorsObject:(AGTAuthor*)value_;

@end

@interface _AGTBook (BookTagsCoreDataGeneratedAccessors)
- (void)addBookTags:(NSSet*)value_;
- (void)removeBookTags:(NSSet*)value_;
- (void)addBookTagsObject:(AGTBookTag*)value_;
- (void)removeBookTagsObject:(AGTBookTag*)value_;

@end

@interface _AGTBook (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveLastRead;
- (void)setPrimitiveLastRead:(NSDate*)value;

- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;

- (NSMutableSet*)primitiveAnnotations;
- (void)setPrimitiveAnnotations:(NSMutableSet*)value;

- (NSMutableSet*)primitiveAuthors;
- (void)setPrimitiveAuthors:(NSMutableSet*)value;

- (NSMutableSet*)primitiveBookTags;
- (void)setPrimitiveBookTags:(NSMutableSet*)value;

- (AGTBookCoverPhoto*)primitiveCoverPhoto;
- (void)setPrimitiveCoverPhoto:(AGTBookCoverPhoto*)value;

- (AGTPDF*)primitivePdf;
- (void)setPrimitivePdf:(AGTPDF*)value;

@end
