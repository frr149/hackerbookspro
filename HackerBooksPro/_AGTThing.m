// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTThing.m instead.

#import "_AGTThing.h"

const struct AGTThingAttributes AGTThingAttributes = {
	.date = @"date",
	.name = @"name",
	.number = @"number",
};

@implementation AGTThingID
@end

@implementation _AGTThing

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Thing" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Thing";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Thing" inManagedObjectContext:moc_];
}

- (AGTThingID*)objectID {
	return (AGTThingID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"numberValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"number"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic date;

@dynamic name;

@dynamic number;

- (int64_t)numberValue {
	NSNumber *result = [self number];
	return [result longLongValue];
}

- (void)setNumberValue:(int64_t)value_ {
	[self setNumber:@(value_)];
}

- (int64_t)primitiveNumberValue {
	NSNumber *result = [self primitiveNumber];
	return [result longLongValue];
}

- (void)setPrimitiveNumberValue:(int64_t)value_ {
	[self setPrimitiveNumber:@(value_)];
}

@end

