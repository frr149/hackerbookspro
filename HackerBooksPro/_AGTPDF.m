// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTPDF.m instead.

#import "_AGTPDF.h"

const struct AGTPDFAttributes AGTPDFAttributes = {
	.data = @"data",
	.urlString = @"urlString",
};

const struct AGTPDFRelationships AGTPDFRelationships = {
	.book = @"book",
};

@implementation AGTPDFID
@end

@implementation _AGTPDF

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PDF" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PDF";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PDF" inManagedObjectContext:moc_];
}

- (AGTPDFID*)objectID {
	return (AGTPDFID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic data;

@dynamic urlString;

@dynamic book;

@end

