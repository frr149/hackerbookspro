//
//  AGTAnnotationViewController.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 20/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTAnnotationViewController.h"
#import "AGTAnnotation.h"
#import "AGTPhotoViewController.h"

@interface AGTAnnotationViewController ()
@property (nonatomic, strong) AGTAnnotation *model;
@end

@implementation AGTAnnotationViewController



-(id) initWithModel:(AGTAnnotation*) model{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = model;
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.textView.text = self.model.text;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    self.model.text = self.textView.text;
}





#pragma mark - Actions
- (IBAction)displayPhoto:(id)sender {
    
    AGTPhotoViewController *pvc = [[AGTPhotoViewController alloc]
                                   initWithModel:self.model.photo];
    [self.navigationController pushViewController:pvc
                                         animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
