// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTTag.m instead.

#import "_AGTTag.h"

const struct AGTTagAttributes AGTTagAttributes = {
	.name = @"name",
	.proxyForSorting = @"proxyForSorting",
};

const struct AGTTagRelationships AGTTagRelationships = {
	.bookTags = @"bookTags",
};

@implementation AGTTagID
@end

@implementation _AGTTag

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Tag";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:moc_];
}

- (AGTTagID*)objectID {
	return (AGTTagID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic proxyForSorting;

@dynamic bookTags;

- (NSMutableSet*)bookTagsSet {
	[self willAccessValueForKey:@"bookTags"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"bookTags"];

	[self didAccessValueForKey:@"bookTags"];
	return result;
}

@end

