//
//  AGTAnnotationViewController.h
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 20/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AGTAnnotation;

@interface AGTAnnotationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *textView;
- (IBAction)displayPhoto:(id)sender;

-(id) initWithModel:(AGTAnnotation*) model;

@end
