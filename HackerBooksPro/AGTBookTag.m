#import "AGTBookTag.h"

@interface AGTBookTag ()

// Private interface goes here.

@end

@implementation AGTBookTag

+(instancetype) bookTagWithBook: (AGTBook*) book
                            tag:(AGTTag*) tag
                        context:(NSManagedObjectContext*) context{
    
    // esta es la propiedad que lo hace único
    NSString *name = [NSString stringWithFormat: @"%@+%@", book.title, tag.name];
    
    AGTBookTag *bt = [AGTBookTag uniqueObjectWithValue:name
                                                forKey:AGTBookTagAttributes.name
                                inManagedObjectContext:context];
    bt.tag = tag;
    bt.book = book;
    
    return bt;
}

@end
