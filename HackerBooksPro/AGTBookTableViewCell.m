//
//  AGTBookTableViewCell.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 14/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#define FAVORITE_KEY @"isFavorite"

#import "AGTBookTableViewCell.h"
#import "AGTBook.h"
#import "AGTBookCoverPhoto.h"


@interface AGTBookTableViewCell()
@property (nonatomic, strong) AGTBook *model;

@end
@implementation AGTBookTableViewCell

#pragma mark -  Class Methods
+(CGFloat) height{
    return 94;
}

+(NSString *)cellId{
    return NSStringFromClass(self);
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)flipFavoriteState:(id)sender {
    
    self.model.isFavorite = !self.model.isFavorite;
}

#pragma mark -  KVO
-(void) observeBook:(AGTBook*) book{
    
    self.model = book;
    
    // Observamos las propiedades que pueden cambiar
    [self.model addObserver:self
                 forKeyPath:AGTBookRelationships.bookTags
                    options:0
                    context:NULL];
    
    [self.model addObserver:self
                 forKeyPath:@"coverPhoto.data"
                    options:0
                    context:NULL];
    
    
    [self syncWithBook];
    
    
    
}

-(void) observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context{
    
    [self syncWithBook];
}




#pragma mark -  Sync
-(void) syncWithBook{
    
    self.titleView.text = self.model.title;
    self.authorsView.text = self.model.stringWithAuthors;
    self.tagsView.text = self.model.stringWithTags;
    
    // Puede cambiar imagen y favoritos
    [UIView transitionWithView:self.coverView
                      duration:0.7
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.coverView.image = self.model.coverPhoto.image;
                    } completion:nil];
    
    
    [self syncFavoriteState];
}

-(void)syncFavoriteState{
    
    UIColor *defaultColor = [UIColor colorWithWhite:0.69
                                                     alpha:1];
    UIColor *favColor = [UIColor colorWithRed:246.0/255.0
                                        green:162.0/255.0
                                         blue:26.0/255.0
                                        alpha:1];
    
    if (self.model.isFavorite) {
        [self.favoriteButton setTitleColor:favColor
                                  forState:UIControlStateNormal];
    }else{
        [self.favoriteButton setTitleColor:defaultColor
                                  forState:UIControlStateNormal];
    }
}




#pragma mark - Cleanup
-(void) prepareForReuse{
    [super prepareForReuse];
    
    
    // hacemos limpieza
    [self cleanUp];
}

-(void) cleanUp{
    
    [self.model removeObserver:self
                    forKeyPath:AGTBookRelationships.bookTags];
    
    [self.model removeObserver:self
                    forKeyPath:@"coverPhoto.data"];
    
    self.model = nil;
    self.coverView.image = nil;
    self.titleView.text = nil;
    self.authorsView.text = nil;
    self.tagsView.text = nil;
}


@end
