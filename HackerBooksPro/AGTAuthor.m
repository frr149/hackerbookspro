#import "AGTAuthor.h"

@interface AGTAuthor ()

// Private interface goes here.

@end

@implementation AGTAuthor

+(instancetype) authorWithFullName:(NSString*) name
                           context:(NSManagedObjectContext *)context{
    
    if ((!name) || [name length] == 0) {
        name = @"Unknown Author";
    }
    
    name = [name capitalizedString];
    
    // Authors should be unique too
    return [self uniqueObjectWithValue:name
                                forKey:AGTAuthorAttributes.fullName
                inManagedObjectContext:context];
    
}

+(NSArray*) arrayOfAuthorsWithArrayOfStrings:(NSArray*) strings
                                     context:(NSManagedObjectContext*) context;
{
    
    // Suponemos que los nombres de los autores estarán siempre
    // bien escritos y no vamos a recibir el mismo autor con la
    // caja cambiada o errores ortográficos:
    // (Juan Pérez vs juan perez, etc...)
    
    NSMutableArray *authors = [NSMutableArray arrayWithCapacity:strings.count];
        
    // Por cada cadena, creamos un autor
    for (NSString *string  in strings) {
        [authors addObject:[AGTAuthor authorWithFullName:string
                                                 context:context]];
    }
    
    return authors;

}



@end
