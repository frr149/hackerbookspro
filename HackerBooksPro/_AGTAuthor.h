// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTAuthor.h instead.

@import CoreData;
#import "AGTBaseManagedObject.h"

extern const struct AGTAuthorAttributes {
	__unsafe_unretained NSString *fullName;
} AGTAuthorAttributes;

extern const struct AGTAuthorRelationships {
	__unsafe_unretained NSString *books;
} AGTAuthorRelationships;

@class AGTBook;

@interface AGTAuthorID : NSManagedObjectID {}
@end

@interface _AGTAuthor : AGTBaseManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AGTAuthorID* objectID;

@property (nonatomic, strong, readonly) NSString* fullName;

//- (BOOL)validateFullName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *books;

- (NSMutableSet*)booksSet;

@end

@interface _AGTAuthor (BooksCoreDataGeneratedAccessors)
- (void)addBooks:(NSSet*)value_;
- (void)removeBooks:(NSSet*)value_;
- (void)addBooksObject:(AGTBook*)value_;
- (void)removeBooksObject:(AGTBook*)value_;

@end

@interface _AGTAuthor (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveFullName;
- (void)setPrimitiveFullName:(NSString*)value;

- (NSMutableSet*)primitiveBooks;
- (void)setPrimitiveBooks:(NSMutableSet*)value;

@end
