//
//  AGTPhotoViewController.h
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 20/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AGTAnnotationPhoto;

@interface AGTPhotoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
- (IBAction)takePicture:(id)sender;

-(id) initWithModel:(AGTAnnotationPhoto*) model;
@end
