#import "AGTBook.h"
#import "AGTTag.h"
#import "AGTBookCoverPhoto.h"
#import "AGTPDF.h"
#import "AGTAuthor.h"
#import "AGTBookTag.h"
#import "NSString+Tokenizing.h"
#import "NSArray+Functional.h"
#import "AGTBookTag.h"

/* JSON Properties */
#define TITLE @"title"
#define AUTHORS @"authors"
#define IMAGE_URL @"image_url"
#define PDF_URL @"pdf_url"
#define TAGS @"tags"


@interface AGTBook ()

// Private interface goes here.

@end

@implementation AGTBook
@synthesize stringWithAuthors=_stringWithAuthors;

#pragma mark - Properties
-(NSString*) stringWithAuthors{
    
    if (_stringWithAuthors == nil) {
        NSArray *a = [[self.authors valueForKey:AGTAuthorAttributes.fullName] allObjects];
        a = [a filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"'Contributors' != self"]];
        a = [a sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        _stringWithAuthors = [a componentsJoinedByString:@", "];
    }
    
    
    return _stringWithAuthors;
}

-(NSString*) stringWithTags{
    
    NSArray *tags = [[[self.bookTags
                         valueForKey:AGTBookTagRelationships.tag]
                      valueForKey:AGTBookTagAttributes.name]
                     allObjects];
    
    tags = [tags sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    return [tags componentsJoinedByString:@", "];
}

#pragma mark - Class Methods
+(instancetype)bookWithTitle:(NSString*) title
                     authors:(NSArray*) authors
                        tags:(NSArray*) tags
                  coverPhoto:(AGTBookCoverPhoto*) coverPhoto
                         pdf:(AGTPDF*) bookPdf
                     context:(NSManagedObjectContext*) context{
    
    AGTBook *b = [AGTBook uniqueObjectWithValue:title
                                         forKey:AGTBookAttributes.title
                         inManagedObjectContext:context];
    
    // Como muchas de las propiedades de Book son readonly desde fuera
    // (realmente no se van a cambiar una vez creado) y NO estamos en
    // la instancia, sino en la clase, nos toca usar KVC
    [b setValue:title
         forKey:AGTBookAttributes.title];
    
    [b setValue:coverPhoto
         forKey:AGTBookRelationships.coverPhoto];
    
    [b setValue:bookPdf
         forKey:AGTBookRelationships.pdf];
    
    
    // Para las propiedades "a muchos" MoGenerator me genera unos métods
    // que acpetan NSSets. Convierto los arrays en NSSets
    [b addAuthors:[NSSet setWithArray:authors]];
    
    // Tengo un array de Tags que tengo que convertir en un
    // array de BookTags y luego en un NSSet
    NSArray *bookTags = [tags arrayWithMappingBlock:^id(id element) {
        return [AGTBookTag bookTagWithBook:b
                                       tag:element
                                   context:context];
    }];
    [b addBookTags:[NSSet setWithArray:bookTags]];
    
    
    return b;
    
}

+(instancetype) bookWithJSONDictionary:(NSDictionary*) dict
                               context:(NSManagedObjectContext*) context{
    
    // Un array de autores a partir de un array de nombres de autor
    NSArray *arrayOfAuthors =[AGTAuthor
                              arrayOfAuthorsWithArrayOfStrings:[[dict objectForKey:AUTHORS] tokenizeByCommas]
                              context:context];
    
    // Un array de tags a partir de un array de nombres de tag
    NSArray *arrayOfTags = [AGTTag
                            arrayOfTagsWithArrayOfStrings:[[dict objectForKey:TAGS] tokenizeByCommas]
                            context:context];
    
    UIImage *defImage = [UIImage imageNamed:@"emptyBookCover.png"];
    AGTBookCoverPhoto *cover = [AGTBookCoverPhoto
                                bookCoverPhotoWithDefaultImage:defImage
                                remotePhotoURL:[NSURL URLWithString:[dict objectForKey:IMAGE_URL]]
                                context:context];
    
    AGTPDF *pdf = [AGTPDF pdfWithRemoteURL:[NSURL URLWithString:[dict objectForKey:PDF_URL]]
                                   context:context];
    
    return [self bookWithTitle:[dict objectForKey:TITLE]
                       authors: arrayOfAuthors
                          tags:arrayOfTags
                    coverPhoto: cover
                           pdf:pdf
                       context:context];
    
}


#pragma mark - Favorite management
-(void) setIsFavorite: (BOOL) newValue{
    
    // Solo hacemos algo si los valores son !=
    if (newValue != self.isFavorite) {
        
        AGTTag *fav = [AGTTag favoriteTagInContext:self.managedObjectContext];
        
        if (newValue) {
            // Hacemos favorito
            
            // Creamos el BookTag: si no era favorito, ahora
            // lo será. Si lo era, no pasa nada.
            [AGTBookTag bookTagWithBook:self
                                    tag:fav
                                context:self.managedObjectContext];

        }else{
            
            // Lo hacemos NO favorito
            
            // Obtengo el BookTag favorito correspondiente y lo destruyo
            AGTBookTag *fb = [AGTBookTag bookTagWithBook:self
                                                     tag:fav
                                                 context:self.managedObjectContext];
            [self.managedObjectContext deleteObject:fb];
            
            // Esto fuerza a Core Data a hacer los cambios en la relaciones
            // que están pendientes. Sino, los haría en el siguiente RunLoop.
            // En general, no debes de estr mandando el mensaje
            // processPendingChanges en vano, o escarallas el rendimiento.
            // Hazlo sólo cuando veas que sin él, la App no funciona
            // correctamente, como en este caso (prueba a quitarlo).
            [self.managedObjectContext processPendingChanges];
            
        }
        
        
        
    }
    
}

-(BOOL) isFavorite{
    
    // No podemos crear el bookTag correspondiente y buscarlo en el set
    // bookTags, por el crearlo ya lo pone ahí (Corolario de Murphy al
    // Principio de Heisemberg aplicado a Core Data).
    
    // Usaremos la propiedad isFavorite de AGTTag
    
    // Obtenemos un set de AGTTags
    NSSet *values = [self.bookTags valueForKey:AGTBookTagRelationships.tag];
    
    // Obtenemos un set de @BOOL:
    // {@YES, @NO} -> habia un Tag con isFavorite
    // {@NO} -> NO habia ningun tag con isFavorite
    values = [values valueForKey:@"isFavorite"];
    
    return [values member:@YES];
    
}



@end
