//
//  AGTFilteredLibraryViewController.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 24/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTFilteredLibraryViewController.h"
#import "AGTBookTag.h"
#import "AGTBookTableViewCell.h"
#import "AGTBookViewController.h"

@interface AGTFilteredLibraryViewController ()

@end

@implementation AGTFilteredLibraryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self registerNib];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    NSInteger n = [[self.fetchedResultsController fetchedObjects] count];
    return n;
}

-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Results";
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    // Obtener el Book
    AGTBook *b = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Crear la celda
    AGTBookTableViewCell *cell = [tableView
                                  dequeueReusableCellWithIdentifier:[AGTBookTableViewCell cellId]
                                  forIndexPath:indexPath];
    
    
    
    // Configurarla
    [cell observeBook:b];
    
    return cell;

}

#pragma mark - Delegate
-(CGFloat) tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [AGTBookTableViewCell height];
}


-(void) tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    // El Book
    AGTBook *b = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    

    // El controlador
    AGTBookViewController *bvc = [[AGTBookViewController alloc] initWithModel:b];
    
    [self.presentingViewController.navigationController pushViewController:bvc
                                         animated:YES];
    
    
}


#pragma mark - Cell
-(void) registerNib{
    
    
    UINib *nib = [UINib nibWithNibName:@"AGTBookTableViewCell"
                                bundle:[NSBundle mainBundle]];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:[AGTBookTableViewCell cellId]];
    
}

@end
