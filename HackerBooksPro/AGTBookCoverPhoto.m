#import "AGTBookCoverPhoto.h"
#import "AGTAsyncImage.h"

@interface AGTBookCoverPhoto () <AGTAsyncImageDelegate>

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) AGTAsyncImage *asyncImage;
;
@end

@implementation AGTBookCoverPhoto

@synthesize asyncImage;

#pragma mark - Properties
-(NSURL*) remoteURL{
    return[NSURL URLWithString: self.remoteURLString];
}

-(UIImage*) image{
    return [UIImage imageWithData:self.data];
}

-(void) setImage:(UIImage *)image{
    self.data = UIImageJPEGRepresentation(image, 0.99);
}


+(instancetype) bookCoverPhotoWithDefaultImage:(UIImage*) defaultImage
                                remotePhotoURL:(NSURL*) remoteURL
                                       context:(NSManagedObjectContext *) context{
    
    // Book cover are unique by the remoteURLString
    AGTBookCoverPhoto *photo = [self uniqueObjectWithValue:[remoteURL absoluteString]
                                                    forKey:AGTBookCoverPhotoAttributes.remoteURLString
                                    inManagedObjectContext:context];
    
    photo.image = defaultImage;

    
    return photo;
    
    
}


#pragma mark -  Life cycle
-(void) awakeFromInsert{
    [super awakeFromInsert];
    
    [self setupKVO];
    
}

-(void) awakeFromFetch{
    [super awakeFromFetch];
    
    [self setupKVO];
    
}

-(void) willTurnIntoFault{
    [super willTurnIntoFault];
    
    [self tearDownKVO];
}

#pragma mark -  AsyncImage
-(void) startAsyncImage{
    
    self.asyncImage = [AGTAsyncImage asyncImageWithURL:self.remoteURL
                                          defaultImage:self.image];
    self.asyncImage.delegate = self;
    
}

#pragma mark -  asyncImage
-(void) asyncImageDidChange:(AGTAsyncImage *)aImage{
    
    // Esto causará una notificación KVO de cambio de la
    // propiedad image de AGTBookCoverPhoto que podrá ser
    // observado por otros objetos.
    self.image = aImage.image;
    
    self.asyncImage = nil;
    
}


#pragma mark - KVO
-(void) setupKVO{
   
    [self addObserver:self
           forKeyPath:AGTBookCoverPhotoAttributes.remoteURLString
              options:0
              context:NULL];
}

-(void) tearDownKVO{
    [self removeObserver:self
              forKeyPath:AGTBookCoverPhotoAttributes.remoteURLString];
}

-(void) observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context{

    // Start downloading the image
    [self startAsyncImage];
    
}
@end
