//
//  NSArray+Functional.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 09/06/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "NSArray+Functional.h"

@implementation NSArray (Functional)

-(NSArray*)arrayWithMappingBlock:(id (^)(id element))mappingBlock{
    
    // No es seguro llamar a un bloque que sea nil
    if (mappingBlock) {
        NSMutableArray *mapped = [NSMutableArray arrayWithCapacity:self.count];
        
        for (id each in self) {
            [mapped addObject:mappingBlock(each)];
        }
        
        return [mapped copy];   // Devolvemos versión inmutable.
    }else{
        return self;
    }
    
}

@end
