#import "AGTAnnotationPhoto.h"

@interface AGTAnnotationPhoto ()

// Private interface goes here.

@end

@implementation AGTAnnotationPhoto

-(void) setImage:(UIImage *)image{
    
    self.data = UIImageJPEGRepresentation(image, 0.99);
}

-(UIImage*) image{
    return [UIImage imageWithData:self.data];
}

+(instancetype)annotationPhotoWithImage:(UIImage *)image context:(NSManagedObjectContext *)context{
    
    AGTAnnotationPhoto *p = [AGTAnnotationPhoto insertInManagedObjectContext:context];
    
    p.image = image;
    
    return p;
}

@end
