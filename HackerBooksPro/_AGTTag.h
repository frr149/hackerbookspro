// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTTag.h instead.

@import CoreData;
#import "AGTBaseManagedObject.h"

extern const struct AGTTagAttributes {
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *proxyForSorting;
} AGTTagAttributes;

extern const struct AGTTagRelationships {
	__unsafe_unretained NSString *bookTags;
} AGTTagRelationships;

@class AGTBookTag;

@interface AGTTagID : NSManagedObjectID {}
@end

@interface _AGTTag : AGTBaseManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AGTTagID* objectID;

@property (nonatomic, strong, readonly) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong, readonly) NSString* proxyForSorting;

//- (BOOL)validateProxyForSorting:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *bookTags;

- (NSMutableSet*)bookTagsSet;

@end

@interface _AGTTag (BookTagsCoreDataGeneratedAccessors)
- (void)addBookTags:(NSSet*)value_;
- (void)removeBookTags:(NSSet*)value_;
- (void)addBookTagsObject:(AGTBookTag*)value_;
- (void)removeBookTagsObject:(AGTBookTag*)value_;

@end

@interface _AGTTag (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveProxyForSorting;
- (void)setPrimitiveProxyForSorting:(NSString*)value;

- (NSMutableSet*)primitiveBookTags;
- (void)setPrimitiveBookTags:(NSMutableSet*)value;

@end
