// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTAnnotationPhoto.m instead.

#import "_AGTAnnotationPhoto.h"

const struct AGTAnnotationPhotoAttributes AGTAnnotationPhotoAttributes = {
	.data = @"data",
};

const struct AGTAnnotationPhotoRelationships AGTAnnotationPhotoRelationships = {
	.annotations = @"annotations",
};

@implementation AGTAnnotationPhotoID
@end

@implementation _AGTAnnotationPhoto

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AnnotationPhoto" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AnnotationPhoto";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AnnotationPhoto" inManagedObjectContext:moc_];
}

- (AGTAnnotationPhotoID*)objectID {
	return (AGTAnnotationPhotoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic data;

@dynamic annotations;

- (NSMutableSet*)annotationsSet {
	[self willAccessValueForKey:@"annotations"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"annotations"];

	[self didAccessValueForKey:@"annotations"];
	return result;
}

@end

