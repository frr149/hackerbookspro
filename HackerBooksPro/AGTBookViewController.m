//
//  AGTBookControllerViewController.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 17/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTBookViewController.h"
#import "AGTBook.h"
#import "AGTSimplePDFViewController.h"
#import "AGTBookCoverPhoto.h"


@interface AGTBookViewController ()

@end

@implementation AGTBookViewController

-(id) initWithModel:(AGTBook*) book{
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = book;
        
        
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Asegurarme de que la vista del controlador ocupa solo el espacio
    // que deje un navigation o un tabBar
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Observamos el libro por KVO: lo único que puede
    // cambiar es el estado favoritos (BookTags)
    [self observeBook];

    // Sincronizamos modelo -> vistas
    [self syncWithModel];
    
    // Si estoy dentro de un SplitVC me pongo el botón
    self.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;

}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    // baja en notificaciones del libro
    [self removeBookObserver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  KVO
-(void) observeBook{

    [self.model addObserver:self
                 forKeyPath:AGTBookRelationships.bookTags
                    options:0
                    context:NULL];
}

-(void) removeBookObserver{
    [self.model removeObserver:self
                    forKeyPath:AGTBookRelationships.bookTags];
}

-(void) observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context{
    
    [self syncWithModel];
    
}

#pragma mark - Actions
- (IBAction)readBook:(id)sender {

    AGTSimplePDFViewController *pdfVC = [[AGTSimplePDFViewController alloc] initWithModel:self.model];
    
    [self.navigationController pushViewController:pdfVC
                                         animated:YES];
}

- (IBAction)flipFavorite:(id)sender {
    self.model.isFavorite = !self.model.isFavorite;
}


#pragma mark - AGTLibraryViewControllerDelegate
-(void) libraryViewController:(AGTLibraryViewController *)vc
                     didSelectBook:(AGTBook *)newBook{
    
    // cambiamos modelo y sincronizamos con el nuevo
    self.model = newBook;
    [self syncWithModel];
}

#pragma mark - UISplitViewControllerDelegate
-(void) splitViewController:(UISplitViewController *)svc willChangeToDisplayMode:(UISplitViewControllerDisplayMode)displayMode{
    
    // Averiguar si la tabla se ve o no
    if (displayMode == UISplitViewControllerDisplayModePrimaryHidden) {
        
        // La tabla está oculta y cuelga del botón
        // Ponemos ese botón en mi barra de navegación
        self.navigationItem.leftBarButtonItem = svc.displayModeButtonItem;
    }else{
        // Se muestra la tabla: oculto el botón de la
        // barra de navegación
        self.navigationItem.leftBarButtonItem = nil;
    }
    
    
}


#pragma mark -  Utils
-(void) syncWithModel{
    
    self.title = self.model.title;
    
    [UIView transitionWithView:self.coverView
                      duration:0.7
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.coverView.image = self.model.coverPhoto.image;
                    } completion:nil];

    
    
    if (self.model.isFavorite) {
        self.favoriteButton.title = @"★";
    }else{
        self.favoriteButton.title = @"☆";
    }
}
@end
