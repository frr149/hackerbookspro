//
//  AGTSimplePDFViewController.m
//  HackerBooks
//
//  Created by Fernando Rodríguez Romero on 17/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTSimplePDFViewController.h"
#import "AGTBook.h"
#import "AGTLibraryViewController.h"
#import "AGTPdf.h"
#import "AGTAnnotationsViewController.h"

@interface AGTSimplePDFViewController ()

@end

@implementation AGTSimplePDFViewController

-(id)initWithModel:(AGTBook*) book{
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _model = book;
        self.title = book.title;
    }
    
    return self;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.activityView.hidden = YES;
    
    [self syncWithModel];
    
    [self addAnnotationsButton];
}




#pragma mark - Util
-(void) addAnnotationsButton{
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc]
                            initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks
                            target:self
                            action:@selector(displayAnnotations:)];
    
    self.navigationItem.rightBarButtonItem = btn;
    
}

-(void) syncWithModel{
    
    NSURL *dummyURL = [NSURL URLWithString:@"http://www.keepcoding.io"];
    
    self.title = self.model.title;
    
    self.activityView.hidden = NO;
    [self.activityView startAnimating];
    [self.model.pdf withDownloadedPDFData:^(NSData *pdfData) {
        
        [self.pdfView loadData:pdfData
                      MIMEType:@"application/pdf"
              textEncodingName:@"UTF-8"
                       baseURL:dummyURL];
        
        [self.activityView stopAnimating];
        self.activityView.hidden = YES;
        
    }];
    
    
}

#pragma mark - Actions
-(void) displayAnnotations:(id) sender{
    
    
    AGTAnnotationsViewController *avc = [[AGTAnnotationsViewController alloc]
                                         initWithModel:self.model];
    
    [self.navigationController pushViewController:avc
                                         animated:YES];
    
}

@end















