// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTBookCoverPhoto.m instead.

#import "_AGTBookCoverPhoto.h"

const struct AGTBookCoverPhotoAttributes AGTBookCoverPhotoAttributes = {
	.data = @"data",
	.remoteURLString = @"remoteURLString",
};

const struct AGTBookCoverPhotoRelationships AGTBookCoverPhotoRelationships = {
	.book = @"book",
};

@implementation AGTBookCoverPhotoID
@end

@implementation _AGTBookCoverPhoto

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BookCoverPhoto" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BookCoverPhoto";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BookCoverPhoto" inManagedObjectContext:moc_];
}

- (AGTBookCoverPhotoID*)objectID {
	return (AGTBookCoverPhotoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic data;

@dynamic remoteURLString;

@dynamic book;

@end

