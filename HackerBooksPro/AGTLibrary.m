//
//  AGTLibrary.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 01/05/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "AGTLibrary.h"
#import "AGTCoreDataStack.h"
#import "Settings.h"
#import "AGTBook.h"
#import "AGTTag.h"
#import "AGTBookTag.h"

@interface AGTLibrary ()
@property (strong, nonatomic)  AGTCoreDataStack *stack;

@property (strong, nonatomic) NSPredicate *titlePredicateTemplate;
@property (strong, nonatomic) NSPredicate *authorsPredicateTemplate;

@end


@implementation AGTLibrary
@synthesize tags = _tags;
@synthesize isOpen = _isOpen;

-(NSManagedObjectContext *) context{
    return self.stack.context;
}

#pragma mark -  Initialization
-(id) init{
    if (self = [super init]) {
        _isOpen = NO;
        [self setupCoreDataStack];
        [self setupTemplatePredicates];
        
    }
    return self;
}

-(void) openWithCompletionHandler:(void (^)(NSError *))completionBlock{
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSError __block *openingError = nil;
    
    // download & import if necessary
    if (![def boolForKey:IS_LIBRARY_IN_CORE_DATA]) {
        
        [self downloadJSONWithContinuationBlock:^(NSError *err, NSArray *jsonDicts) {
            
            //
            // ¡OJO! Aquí estamos en primer plano por dos razones:
            // a) El importado es muy rápido porque Core data es muy rápido y
            //    los datos son muy pocos.
            // b) Todavía no sabemos usar Core data en segundo plano
            //
            if (!err) {
                // No error, so init the stack and then import
                [self importFromJSONData:jsonDicts];
                [def setBool:YES
                      forKey:IS_LIBRARY_IN_CORE_DATA];
            }else{
                
                // Something went horribly wrong.
                openingError = err;
            }
            
            // AutoSave
            [self autoSave];
            
            // Run completionBlock
            _isOpen = YES;
            completionBlock(openingError);

        }];
    }
}


#pragma mark - Downloading
/*
 * Método asíncrono que ejecuta su bloque de continuación
 * en primer plano.
 */
-(void)downloadJSONWithContinuationBlock:(void (^)(NSError*err, NSArray *jsonDicts))continuation{
    
    
    
    dispatch_sync(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        
        // Ya estamos en segundo plano
        NSError * error = nil;
        NSArray *jsonArray = nil;
        
        // Posible error
        NSString *description = [NSString stringWithFormat:@"Error while downloading the json data from %@. Expect an array of dictionaries and got nil instead.", JSON_URL_STRING];
        
        // Descargamos
        NSData *jsonData = [NSData dataWithContentsOfURL:[NSURL URLWithString:JSON_URL_STRING]];
        
        // Comprobamos resultado
        if (!jsonData) {
            error = [NSError errorWithDomain:@"HackerBooksPro.Model"
                                        code:42
                                    userInfo:@{NSLocalizedDescriptionKey : description}];
        }else{
            
            // Lo parseamos a objetos cocoa
            id jsonObjs = [NSJSONSerialization JSONObjectWithData:jsonData
                                                          options:NSJSONReadingMutableContainers
                                                            error:&error];
            
            if (![jsonObjs isKindOfClass:[NSArray class]]) {
                // We were expecting an array of dicts.
                error = [NSError errorWithDomain:@"HackerBooksPro.Model"
                                            code:43
                                        userInfo:@{NSLocalizedDescriptionKey : description}];
            }
            else{
                // Todo bien
                jsonArray = jsonObjs;
            }
        }
        
        
        // A primer plano
        dispatch_async(dispatch_get_main_queue(), ^{
            continuation(error,jsonArray);
        });
    });
    
}



#pragma mark - Core Data & Import
-(void) setupCoreDataStack{
    
    self.stack = [AGTCoreDataStack coreDataStackWithModelName:@"Model"];
}

/*
 * Convierte los dicts de JSON en objetos de Core Data (AGTBooks)
 * y los guarda
 */
-(void)importFromJSONData:(NSArray *)jsonDicts{
    
    // Antes de empezar, borramos lo que puediese haber
    [self.stack zapAllData];
    
    
    for (NSDictionary *dict in jsonDicts) {
        [AGTBook bookWithJSONDictionary:dict
                                context:self.stack.context];
    }
    
    [self.stack saveWithErrorBlock:^(NSError *error) {
        NSLog(@"Error al guardar %@", error);
        
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        [def setBool:NO forKey:IS_LIBRARY_IN_CORE_DATA];
        
    }];
    
    
    
    
}






#pragma mark - Fetching
-(NSFetchRequest*) bookTagsFetchRequest{
    
    NSFetchRequest *r = [NSFetchRequest fetchRequestWithEntityName:[AGTBookTag entityName]];
    r.fetchBatchSize = 30;
    r.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"tag.proxyForSorting"
                                                        ascending:YES
                                                         selector:@selector(caseInsensitiveCompare:)],
                          [NSSortDescriptor sortDescriptorWithKey:@"tag.name"
                                                        ascending:YES
                                                         selector:@selector(caseInsensitiveCompare:)],
                          [NSSortDescriptor sortDescriptorWithKey:@"book.title"
                                                        ascending:YES
                                                         selector:@selector(caseInsensitiveCompare:)]
                          ];
    
    return r;
    
}

-(NSFetchRequest*) booksFetchRequest{
    
    
    NSFetchRequest *r = [NSFetchRequest fetchRequestWithEntityName:[AGTBook entityName]];
    r.fetchBatchSize = 30;
    r.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:AGTBookAttributes.title
                                                        ascending:YES
                                                         selector:@selector(caseInsensitiveCompare:)],
                          ];
    
    return r;
    
    
}
-(NSFetchRequest*) filteredBooksFetchRequestContainingText:(NSString *) text{
    
    if (text || ([text length] > 0)){
        // Predicados específicos
        NSPredicate *title = [self.titlePredicateTemplate
                              predicateWithSubstitutionVariables:@{@"text" : text}];
        
            NSPredicate *authors = [self.authorsPredicateTemplate
                                    predicateWithSubstitutionVariables:@{@"text" : text}];
        
            NSCompoundPredicate *all = [NSCompoundPredicate
                                        orPredicateWithSubpredicates:@[title, authors]];
        
        // FetchRequest
        NSFetchRequest *r = [self booksFetchRequest];
        r.predicate = all;
        r.returnsDistinctResults = YES;
        
        return r;

    }else{
        return [self booksFetchRequest];
    }
}

#pragma mark - Search & predicates
-(void) setupTemplatePredicates{
    
    // Estos actúan como plantillas y sólo sustituimos el valor
    // de la variable $text sin tener que reparsear el predicado.
    // Se hace así por eficiencia.
    self.titlePredicateTemplate = [NSPredicate
                                   predicateWithFormat:@"title CONTAINS[cd] $text"];
    
    self.authorsPredicateTemplate = [NSPredicate
                                     predicateWithFormat:@"ANY authors.fullName CONTAINS[cd] $text"];
}
#pragma mark - AutoSavib
-(void) autoSave{
    
    if (AUTO_SAVE) {
        
        NSLog(@"Autosaving");
        
        [self.stack saveWithErrorBlock:^(NSError *error) {
            NSLog(@"Error while saving %@", error);
        }];
        
        [self performSelector:_cmd
                   withObject:nil
                   afterDelay:AUTO_SAVE_DELAY];
    }
}










@end
