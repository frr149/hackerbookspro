//
//  NSUserDefaults+Reset.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 29/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "NSUserDefaults+Reset.h"

@implementation NSUserDefaults (Reset)

-(void) resetAplicationDomainValues{
    
    // Obtain the name of the Application Domain
    NSString *appDomainName = [[NSBundle mainBundle]bundleIdentifier];
    
    // Remove it
    [self removePersistentDomainForName:appDomainName];
    [self synchronize];
    
}
@end
