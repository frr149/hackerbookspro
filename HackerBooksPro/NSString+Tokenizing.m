//
//  NSString+Tokenizing.m
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 29/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

#import "NSString+Tokenizing.h"

@implementation NSString (Tokenizing)

// Returns an NSArray with all components trimmed of whitespaces
-(NSArray*) tokenizeByCommas{
    
    NSArray *tokens = [self componentsSeparatedByString:@","];
    NSMutableArray *clean = [NSMutableArray arrayWithCapacity:tokens.count];
    
    for (NSString *token in tokens) {
        [clean addObject:[token stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]]];
    }
    
    return clean;
}

@end
