// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTAnnotationPhoto.h instead.

@import CoreData;
#import "AGTBaseManagedObject.h"

extern const struct AGTAnnotationPhotoAttributes {
	__unsafe_unretained NSString *data;
} AGTAnnotationPhotoAttributes;

extern const struct AGTAnnotationPhotoRelationships {
	__unsafe_unretained NSString *annotations;
} AGTAnnotationPhotoRelationships;

@class AGTAnnotation;

@interface AGTAnnotationPhotoID : NSManagedObjectID {}
@end

@interface _AGTAnnotationPhoto : AGTBaseManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AGTAnnotationPhotoID* objectID;

@property (nonatomic, strong) NSData* data;

//- (BOOL)validateData:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *annotations;

- (NSMutableSet*)annotationsSet;

@end

@interface _AGTAnnotationPhoto (AnnotationsCoreDataGeneratedAccessors)
- (void)addAnnotations:(NSSet*)value_;
- (void)removeAnnotations:(NSSet*)value_;
- (void)addAnnotationsObject:(AGTAnnotation*)value_;
- (void)removeAnnotationsObject:(AGTAnnotation*)value_;

@end

@interface _AGTAnnotationPhoto (CoreDataGeneratedPrimitiveAccessors)

- (NSData*)primitiveData;
- (void)setPrimitiveData:(NSData*)value;

- (NSMutableSet*)primitiveAnnotations;
- (void)setPrimitiveAnnotations:(NSMutableSet*)value;

@end
