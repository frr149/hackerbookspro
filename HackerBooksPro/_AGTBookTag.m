// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTBookTag.m instead.

#import "_AGTBookTag.h"

const struct AGTBookTagAttributes AGTBookTagAttributes = {
	.name = @"name",
};

const struct AGTBookTagRelationships AGTBookTagRelationships = {
	.book = @"book",
	.tag = @"tag",
};

@implementation AGTBookTagID
@end

@implementation _AGTBookTag

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BookTag" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BookTag";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BookTag" inManagedObjectContext:moc_];
}

- (AGTBookTagID*)objectID {
	return (AGTBookTagID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic name;

@dynamic book;

@dynamic tag;

@end

