// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTAuthor.m instead.

#import "_AGTAuthor.h"

const struct AGTAuthorAttributes AGTAuthorAttributes = {
	.fullName = @"fullName",
};

const struct AGTAuthorRelationships AGTAuthorRelationships = {
	.books = @"books",
};

@implementation AGTAuthorID
@end

@implementation _AGTAuthor

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Author" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Author";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Author" inManagedObjectContext:moc_];
}

- (AGTAuthorID*)objectID {
	return (AGTAuthorID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic fullName;

@dynamic books;

- (NSMutableSet*)booksSet {
	[self willAccessValueForKey:@"books"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"books"];

	[self didAccessValueForKey:@"books"];
	return result;
}

@end

