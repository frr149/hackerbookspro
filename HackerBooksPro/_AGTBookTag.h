// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTBookTag.h instead.

@import CoreData;
#import "AGTBaseManagedObject.h"

extern const struct AGTBookTagAttributes {
	__unsafe_unretained NSString *name;
} AGTBookTagAttributes;

extern const struct AGTBookTagRelationships {
	__unsafe_unretained NSString *book;
	__unsafe_unretained NSString *tag;
} AGTBookTagRelationships;

@class AGTBook;
@class AGTTag;

@interface AGTBookTagID : NSManagedObjectID {}
@end

@interface _AGTBookTag : AGTBaseManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AGTBookTagID* objectID;

@property (nonatomic, strong, readonly) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AGTBook *book;

//- (BOOL)validateBook:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) AGTTag *tag;

//- (BOOL)validateTag:(id*)value_ error:(NSError**)error_;

@end

@interface _AGTBookTag (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (AGTBook*)primitiveBook;
- (void)setPrimitiveBook:(AGTBook*)value;

- (AGTTag*)primitiveTag;
- (void)setPrimitiveTag:(AGTTag*)value;

@end
