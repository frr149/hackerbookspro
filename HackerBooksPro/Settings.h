//
//  Settings.h
//  HackerBooksPro
//
//  Created by Fernando Rodríguez Romero on 28/04/15.
//  Copyright (c) 2015 Agbo. All rights reserved.
//

// Caches
#define FLUSH_ALL_ASYNC_IMAGE_CACHE YES

// NSUserDefaults
#define FLUSH_APPLICATION_DOMAIN_IN_NSUSERDEFAULTS YES

// Model
#define JSON_URL_STRING @"https://t.co/K9ziV0z3SJ"
#define IS_LIBRARY_IN_CORE_DATA @"IS_MODEL_PRELOADED"


// Auto save
#define AUTO_SAVE NO
#define AUTO_SAVE_DELAY 5

