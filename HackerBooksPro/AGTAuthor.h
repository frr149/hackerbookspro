#import "_AGTAuthor.h"

@interface AGTAuthor : _AGTAuthor {}

+(instancetype) authorWithFullName:(NSString*) name
                           context:(NSManagedObjectContext *)context;

/*
 * Gets an array of author names (as strings) and returns an array
 * of Author objects.
 */
+(NSArray*) arrayOfAuthorsWithArrayOfStrings:(NSArray*) strings
                                     context:(NSManagedObjectContext*) context;


@end
