@import UIKit;
#import "_AGTAnnotationPhoto.h"

@interface AGTAnnotationPhoto : _AGTAnnotationPhoto {}


@property (strong, nonatomic) UIImage *image;


+(instancetype) annotationPhotoWithImage:(UIImage*) image
                                 context:(NSManagedObjectContext*) context;;

@end
