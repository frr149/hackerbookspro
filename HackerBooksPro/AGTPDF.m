#import "AGTPDF.h"

@interface AGTPDF ()

// Private interface goes here.

@end

@implementation AGTPDF


+(instancetype)pdfWithRemoteURL:(NSURL *)url
                        context:(NSManagedObjectContext *)context{
    
    return [self uniqueObjectWithValue:[url absoluteString]
                                forKey:AGTPDFAttributes.urlString
                inManagedObjectContext:context];
}


-(void) withDownloadedPDFData:(void (^)(NSData *pdfData))completionBlock{
    
    // si no está en local
    if (!self.data) {
        
        // descargamos en segundo plano
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            
            NSData *d = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.urlString]];
            
            // A primer plano para el bloque de finalización
            dispatch_async(dispatch_get_main_queue(), ^{
                self.data = d;
                completionBlock(d);
            });
        });
        

    }else{
        // Está en local
        completionBlock(self.data);

    }
    
   
}


@end
