// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AGTThing.h instead.

@import CoreData;
#import "AGTBaseManagedObject.h"

extern const struct AGTThingAttributes {
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *number;
} AGTThingAttributes;

@interface AGTThingID : NSManagedObjectID {}
@end

@interface _AGTThing : AGTBaseManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AGTThingID* objectID;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* number;

@property (atomic) int64_t numberValue;
- (int64_t)numberValue;
- (void)setNumberValue:(int64_t)value_;

//- (BOOL)validateNumber:(id*)value_ error:(NSError**)error_;

@end

@interface _AGTThing (CoreDataGeneratedPrimitiveAccessors)

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveNumber;
- (void)setPrimitiveNumber:(NSNumber*)value;

- (int64_t)primitiveNumberValue;
- (void)setPrimitiveNumberValue:(int64_t)value_;

@end
