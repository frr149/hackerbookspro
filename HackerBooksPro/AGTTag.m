

#define FAVORITE_TAG @"Favorite"



#import "AGTTag.h"
#import "AGTBook.h"
#import "AGTBookTag.h"

@interface AGTTag ()

// Private interface goes here.

@end

@implementation AGTTag

#pragma mark - Properties
-(NSUInteger) bookCount{
    // Cantidubi de libros que tiene este tag
    
    // Con esto elimino los libros repetidos
    NSSet *books = [self.bookTags valueForKey:AGTBookTagRelationships.book];
    
    return books.count;
}

-(BOOL)isFavorite{
    return [self.name isEqualToString:FAVORITE_TAG];
}

+(instancetype) tagWithName:(NSString*) name
                    context:(NSManagedObjectContext*) context{
    
    // BookTags should be unique, so we use the unique (findOrCreate)
    // method in our base class
    AGTTag *tag =  [self uniqueObjectWithValue:[name capitalizedString]
                                        forKey:AGTTagAttributes.name
                        inManagedObjectContext:context];
    
    // proxyForComparison makes sure that Favorite always comes first
    // Uso KVC para saltarme la propiedad readOnly de proxyForSorting
    if ([tag.name isEqualToString:FAVORITE_TAG]) {
        [tag setValue:[NSString stringWithFormat:@"__%@", tag.name]
               forKey:AGTTagAttributes.proxyForSorting];
    }else{
        [tag setValue:tag.name
               forKey:AGTTagAttributes.proxyForSorting];
    }
    
    return tag;
}

#pragma mark - Class Methods

+(instancetype) favoriteTagInContext:(NSManagedObjectContext *)context{
    return [self tagWithName:FAVORITE_TAG
                     context:context];
}


+(NSArray*) arrayOfTagsWithArrayOfStrings:(NSArray*) strings
                                  context:(NSManagedObjectContext*) context{
    
    NSMutableArray *tags = [NSMutableArray arrayWithCapacity:strings.count];
    
    // Vamos creando los tags por cada cadena
    for (NSString *tagName in strings) {
        [tags addObject:[AGTTag tagWithName:tagName
                                    context:context]];
    }
    
    return tags;
}


#pragma mark - Comparison
-(NSComparisonResult) compare:(AGTTag*)other{
    return [self.proxyForSorting caseInsensitiveCompare:other.proxyForSorting];
}







@end
