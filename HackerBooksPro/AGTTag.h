#import "_AGTTag.h"

@class AGTBook;

@interface AGTTag : _AGTTag {}
@property (nonatomic, readonly) BOOL isFavorite;
@property (nonatomic, readonly) NSUInteger bookCount;


+(instancetype) tagWithName:(NSString*) name
                    context:(NSManagedObjectContext*) context;

+(instancetype) favoriteTagInContext:(NSManagedObjectContext*) context;

/*
 * Gets an array of tag names (as strings) and returns an array
 * of BookTag objects.
 */
+(NSArray*) arrayOfTagsWithArrayOfStrings:(NSArray*) strings
                                  context:(NSManagedObjectContext*) context;



-(NSComparisonResult) compare:(AGTTag*)other;




@end
