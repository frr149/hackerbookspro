#import "_AGTBook.h"


@class AGTPDF;
@class AGTBookCoverPhoto;


@interface AGTBook : _AGTBook {}
// Custom logic goes here.

+(instancetype)bookWithTitle:(NSString*) title
                     authors:(NSArray*) authors
                        tags:(NSArray*) tags
                  coverPhoto:(AGTBookCoverPhoto*) coverPhoto
                         pdf:(AGTPDF*) bookPdf
                     context:(NSManagedObjectContext*) context;

+(instancetype) bookWithJSONDictionary:(NSDictionary*) dict
                               context:(NSManagedObjectContext*) context;

@property (nonatomic) BOOL isFavorite;
@property (nonatomic, readonly) NSString *stringWithAuthors;
@property (nonatomic, readonly) NSString *stringWithTags;

@end
